import {Component, Input, OnInit} from '@angular/core';
import {ProgressBarModel} from "../../models/progress-bar.model";

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit {
  @Input() bar: ProgressBarModel;
  @Input() isActive: boolean;

  constructor() {
  }

  ngOnInit() {
  }

}
