import {Component, OnInit} from '@angular/core';
import {ProgressBarConfigService} from './services/progress-bar-config.service';
import {ProgressBarModel} from './models/progress-bar.model';
import {DeviceDetectorService} from 'ngx-device-detector';
import {Observable} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isLoaded = false;
  isFetch = false;
  title = 'Progress Bars Demo';
  config: any;
  bars: ProgressBarModel[] = [];
  currentBar: ProgressBarModel;
  buttons = [];
  barColors = ['#C5EE64', '#E2793F', '#E09D8B', '#EFC94C', '#C1C8E3'];
  isMobile = false;
  dataObservable: Observable<any>;

  constructor(private configService: ProgressBarConfigService, private deviceService: DeviceDetectorService) {
  }

  ngOnInit() {
    this.isMobile = !this.deviceService.isDesktop();
    this.getConfig();
  }

  getConfig() {

    this.isLoaded = false;
    this.isFetch = false;
    this.bars = [];
    this.buttons = [];
    this.config = null;
    this.dataObservable = this.configService.get();
    this.dataObservable.toPromise().then(config => {
        this.config = config;
        this.buttons = config.buttons.map(b => {
          return Math.round((b * 100 / config.limit));
        });
        config.bars.forEach((el, index) => {
          const loadedValue = Math.round(el * 100 / config.limit);
          const bar = new ProgressBarModel(this.barColors[index], loadedValue, config.limit, (index + 1).toString());
          this.bars.push(bar);
        });
        this.currentBar = this.bars[0];
        this.isLoaded = true;
        this.isFetch = true;
      },
      error => {
        console.log(error);
        this.isLoaded = true;
        this.isFetch = false;
      });

  }

  makeBarChange(buttonValue) {
    this.currentBar.setLoadedValue(buttonValue);
  }

  refreshData() {
    this.getConfig();
  }
}
