import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class ProgressBarConfigService {
  private apiEndpoint = environment.baseAPIUrl + 'bars';

  constructor(private http: HttpClient) {
  }

  /** Return an Observable to simulate response from the server */
  get() {
    return this.http.get(this.apiEndpoint);
  }
}
