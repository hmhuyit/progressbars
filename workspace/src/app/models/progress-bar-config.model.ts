export class ProgressBarConfigModel {
  buttons: number[];
  bars: number[];
  limit: number;
}

