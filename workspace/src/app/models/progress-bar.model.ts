export class ProgressBarModel {

  constructor(public color: string,
              public loadedValue: number,
              public limited: number,
              public name: string) {
  }

  setLoadedValue(inputValue) {
    const result = this.loadedValue + inputValue;
    if (result < 0) {
      this.loadedValue = 0;
    } else {
      this.loadedValue = result;
    }
  }

  getBarColor() {
    return this.loadedValue > 100 ? '#DF4949' : this.color;
  }
}
