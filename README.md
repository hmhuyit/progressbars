# Workspace

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.21.
Please install or upgrade Nodejs to version greater than 10.x:  
$ sudo npm install npm@latest -g  
$ sudo npm cache clean -f  
$ sudo npm install -g n  
$ sudo n stable  

## Branches
master and developer - latest source code
release - include `node_modules` and built project in `my-app` folder (do not have to install packages)

## Install all required packages by NPM
run `npm install` in directory `progress/workspace`

## Run App on Development server

Run `npm run-script ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Mobile and tablet responsiveness

Switch the browser to Mobile/Tablet mode and refresh the web page to see the responsiveness.

## Bundle and build project

Run `npm run-script ng build` to bundle all script file in order to build the project. The build artifacts will be stored in the `progressbars/my-app` directory
